# Dotfiles

## Installation and Required Packages

Clone the project

     git clone --recurse-submodules  git@gitlab.com:begui/dotfiles.git

Navigate to the **dotfiles** directory and run the following

     ln -sf `pwd`/vim $HOME/.vim
     ln -sf `pwd`/vim/vimrc $HOME/.vimrc

See Issues:

     ln -sf `pwd`/vim/clang-format $HOME/.clang-format

**MSYS2** will require you to create the symbolic links via Windoze CMD as administrator ( Apparently MSYS2 copies the directory data when creating a symbolic link vis ln )

    mklink .vim path\to\dotfiles\vim
    mklink .vimrc path\to\dotfiles\vimrc
    mklink .clang-format path\to\dotfiles\vim\clang-format


Install the requires libs and applications by running the setup scipts defined in

    scripts/

## Vim

Currently using Vundle.

## NERDTree

Nerdtree Left/Right/Up/Down

     Shift-h/Shift-l/Shift-k/Shift-j

Show/Hide hidden files / directory

     Shift-i

### Vim Sensible

Clear search by highlighting

     Ctl-L

### Vim Autoformat

Autoformat

     F3

### Tagbar

Toggle Tagbar

     F8

### YouCompleteMe

You will need to navigate to the **youcompletme** directory and run the following command

     python3 install.py --clangd-completer

To get this to work with cmake add the following to your **CMakeLists.txt**

     SET( CMAKE_EXPORT_COMPILE_COMMANDS ON )

**Ubuntu 20.04 Note:**

if you have issues with the YCM failing to find core header files, you will need to do the following

Verify your GCC and clang install versions.

I had an issue where i had clang 10 and gcc 9 installed. After installing gcc 10, this includes the version 10 header files located

     /usr/include/c++/10

After this, YCM was no longer complaining about this missing header files

## Issues

I have my source files located on a secondary drive. Due to my setup I had to come up with a workaround to get vim-autoformat to read my **.clang-format** file since it wasn't reading it in **$HOME/.clang-format**

     ln -sf /media/Bakup/Development $HOME/Development
     ln -sf $HOME/Development/dotfiles/vim/clang-format $HOME/Development/.clang-format


