#!/bin/bash

BITBUCKET_HTTP_URL=
BITBUCKET_HTTP_TIMEOUT=10

check_program_exists() {
    if ! type "$1" &> /dev/null; then
        printf "Please install '%s'." "${1}" >&2
        return 1
    fi
    return 0
}

curl_bitbucket() {
  local loc_PATH=$1
  local loc_BB_ACCESS_TOKEN=$2
  curl -w "%{http_code}" --connect-timeout "$BITBUCKET_HTTP_TIMEOUT" -H "Authorization: Bearer $loc_BB_ACCESS_TOKEN" "$BITBUCKET_HTTP_URL/rest/api/latest/$loc_PATH"
  return 0;
}

curl_bitbucket_project_repo() {
  curl_bitbucket "projects/$1/repos?limit=1000" $2
  return 0;
}

clone_or_update_projects() {
  local loc_PROJECT=$1
  local loc_BB_ACCESS_TOKEN=$2
  mkdir -p "$1"
  RESPONSE=$(curl_bitbucket_project_repo "$loc_PROJECT" "$loc_BB_ACCESS_TOKEN")
  RESPONSE_CODE="${RESPONSE: -3}"
  if [[ $RESPONSE_CODE != 2?? ]]; then
    echo "The request returned an HTTP error with code $RESPONSE_CODE"
    return 1
  fi
  PROJECT_LIST_RESPONSE="${RESPONSE::-3}"
  REPOS=$(echo $PROJECT_LIST_RESPONSE | jq '.values[] | . as $value | .links.clone[] | select(.name == "ssh") | { repoName: $value.name, repoType: .name, repoHref: .href }')

  echo $REPOS| jq  -r
  # # Loop through the list of repositories and print each one
  for repo in $(echo $REPOS | jq -c); do
    echo $repo
    repoName=$(echo $repo | jq -r '.repoName')
    repoHref=$(echo $repo | jq -r '.repoHref')

    echo "Repo Name: $repoName"
    echo "Repo Href: $repoHref"

    directory=$(echo "$1/$repoName")
    echo $directory
    if [ -d "$directory" ]; then
      echo "Directory exists"
      (cd $directory && git pull --prune )
    else
    echo
      (cd $1 && git clone $repoHref)
    fi

    echo "-------"
  done

  return 0
}

## Main
check_program_exists 'git'  || exit $?
check_program_exists 'curl' || exit $?
check_program_exists 'jq'   || exit $?


clone_or_update_projects 'Project1' 'Token1'
clone_or_update_projects 'Project2' 'Token2'
