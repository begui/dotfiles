#!/bin/bash
 
IMAGES=`docker images -a -q`
 
if [ "${IMAGES}" != '' ]; then
  docker rmi -f ${IMAGES}
 
  docker images purge
fi
 
CONTAINERS=`docker ps -a -q`
 
if [ "${CONTAINERS}" != '' ]; then
  docker stop ${CONTAINERS}
 
  docker rm -f --volumes ${CONTAINERS}
 
  docker container prune
fi
 
VOLUMES=`docker volume ls -q`
 
if [ "${VOLUMES}" != '' ]; then
  docker volume rm -f ${VOLUMES}
 
  docker volume prune
fi
 
NETWORKS=`docker network ls -q`
 
if [ "${NETWORKS}" != '' ]; then
  docker network prune -f >/dev/null 1>/dev/null 2>/dev/null
 
  docker network rm ${NETWORKS} >/dev/null 1>/dev/null 2>/dev/null
fi
 
docker system prune -a -f --volumes
 
exit 0
