#!/bin/bash

echo "========================================"
echo "          Hardware Information          "
echo "========================================"

# Function to check if a command exists
check_command() {
    if ! command -v "$1" &>/dev/null; then
        echo "Command '$1' is not installed. Please install it to get this information."
        return 1
    fi
    return 0
}

# Check if dmidecode is available once
DMIDECODE_OUTPUT=""
if check_command dmidecode; then
    echo "Collecting dmidecode data..."
    DMIDECODE_OUTPUT=$(sudo dmidecode)
fi

# CPU Information
echo ""
echo "CPU Information:"
if check_command lscpu; then
    lscpu | grep -E 'Model name|Vendor ID|Architecture|CPU(s)'
fi

# GPU Information
echo ""
echo "GPU Information:"
if check_command lspci; then
    lspci | grep -i --color 'vga\|3d\|2d'
fi

# Motherboard Information
echo ""
echo "Motherboard Information:"
if [ -n "$DMIDECODE_OUTPUT" ]; then
    echo "$DMIDECODE_OUTPUT" | grep -A3 "Base Board Information" | grep -E 'Manufacturer|Product Name|Version'
else
    echo "dmidecode is required to fetch motherboard information."
fi

# RAM Information
echo ""
echo "RAM Information:"
if [ -n "$DMIDECODE_OUTPUT" ]; then
    echo "$DMIDECODE_OUTPUT" | grep -A16 "Memory Device" | grep -E 'Size|Speed|Type|Manufacturer|Locator'
else
    echo "dmidecode is required to fetch RAM information."
fi

# Disk Information
echo ""
echo "Disk Information:"
if check_command lsblk; then
    lsblk -o NAME,SIZE,TYPE,MOUNTPOINT
fi

# Network Adapters
echo ""
echo "Network Adapters:"
if check_command lspci; then
    lspci | grep -i --color 'network'
fi

# General System Information
echo ""
echo "General System Information:"
cat /etc/os-release
uname -r

# BIOS/UEFI Information
echo ""
echo "BIOS/UEFI Information:"
if [ -n "$DMIDECODE_OUTPUT" ]; then
    echo "$DMIDECODE_OUTPUT" | grep -A3 "BIOS Information" | grep -E 'Vendor|Version|Release Date'
else
    echo "dmidecode is required to fetch BIOS/UEFI information."
fi

# Detailed Storage Information
echo ""
echo "Detailed Storage Information:"
if check_command lsblk; then
    lsblk -o NAME,SIZE,TYPE,MOUNTPOINT,FSTYPE,MODEL
fi

# GPU Extended Information
echo ""
echo "Detailed GPU Information (if applicable):"
if check_command glxinfo; then
    glxinfo | grep -i 'device'
else
    echo "'glxinfo' is not installed. Please install 'mesa-utils' for detailed GPU information."
fi

# Vulkan Information
if check_command vulkaninfo; then
    vulkaninfo | grep -i 'GPU'
else
    echo "'vulkaninfo' is not installed. Please install it for Vulkan GPU information."
fi

# USB Devices
echo ""
echo "USB Devices:"
if check_command lsusb; then
    lsusb
else
    echo "'lsusb' is not installed. Please install 'usbutils' to view USB devices."
fi

# Battery Information
echo ""
echo "Battery Information:"
if check_command upower; then
    upower -i $(upower -e | grep BAT)
else
    echo "'upower' is not installed. Please install it for battery details."
fi

# Network Details
echo ""
echo "Network Details:"
if check_command ip; then
    ip a
fi

# Audio Information
echo ""
echo "Audio Devices:"
if check_command lspci; then
    lspci | grep -i audio
fi

# Temperature and Fan Speed
echo ""
echo "Temperature and Fan Speed:"
if check_command sensors; then
    sensors
else
    echo "'sensors' is not installed. Please install 'lm-sensors' to view temperature and fan speed."
fi

# PCI Device Tree
echo ""
echo "PCI Devices:"
if check_command lspci; then
    lspci -v
fi

# System Uptime
echo ""
echo "System Uptime:"
if check_command uptime; then
    uptime
fi

echo ""
echo "Done!"

