#!/bin/bash
#
# Drop this into your GW folder with the source checked out
#

bPortPrefix=85
pPortPrefix=81
cPortPrefix=80
aPortPrefix=82


# Define help function
function help(){
    echo "Usage example:";
    echo "centers_kickstart [(-h|--help)] [(-b|--billing)] [(-p|--policy)] [(-c|--claim)] [(-a|--contact)] [(-g|--git)] [(-d|--delete)] [(-i|--ide)] [(-r|--run) string] [(-s|--stop) string] [(-l|--listening) integer]";
    echo "Options:";
    echo "-h or --help: Displays this information.";
    echo "-b or --billing: Run with Billing.";
    echo "-p or --policy: Run with Policy.";
    echo "-c or --claim: Run with Claim.";
    echo "-a or --Contact: Run with Contact Manager.";
    echo "-g or --git: Pull Git code.";
    echo "-d or --delete: Delete local branches that have been removed from remote.";
    echo "-i or --ide: Opens IDE.";
    echo "-r or --run string: Runs server without ide.";
    echo "-s or --stop string: Stops server.";
    echo "-l or --listening integer: Two Digit Port to listen on.";
    exit 1;
}
 
# Declare vars. Flags initalizing to false.
all=true;
billing=false;
contact=false;
claims=false;
policy=false;
git=false;
delete=false;
ide=false;

 

# Execute getopt
ARGS=$(getopt -o "hbpcagdir:s:l:" -l "help,billing,policy,claim,Contact,git,delete,ide,run:,stop:,listening:" -n "centers_kickstart" -- "$@");
 
#Bad arguments
if [ $? -ne 0 ];
then
    help;
fi
 
eval set -- "$ARGS";
 
while true; do
    case "$1" in
        -h|--help)
            shift;
            help;
            ;;
        -b|--billing)
            shift;
                    billing=true;
                    all=false;
            ;;
        -a|--contact)
            shift;
                    contact=true;
                    all=false;
            ;;
        -c|--claims)
            shift;
                    claims=true;
                    all=false;
            ;;
        -p|--policy)
            shift;
                    policy=true;
                    all=false;
            ;;
        -d|--delete)
            shift;
                    delete=true;
            ;;
		-g|--git)
            shift;
                    git=true;
            ;;
        -i|--ide)
            shift;
                    ide=true;
            ;;
		-r|--run)
		   shift;
				if [ -n "$1" ]; 
				then
					run="$1";
					ide=false
					shift;
				fi
            ;;
		-s|--stop)
		   shift;
				if [ -n "$1" ]; 
				then
					stop="$1";
					ide=false
					shift;
				fi
            ;;
	    -l|--listening)
            shift;
				if [ -n "$1" ]; 
				then
					listening="$1";
					shift;
				fi
            ;;
        --)
            shift;
            break;
            ;;
    esac
done
 

###
#
# Start of Main
#
###

declare -a arr=("billingcenter" "contactmanager" "policycenter" "claimcenter")


function pullFromGit () {
	echo -e "Pulling From $1"
	(cd $1 && git pull && git submodule foreach git pull )
}

function openStudio() {
	echo -e "Opening Project $1"
	(cd $1 && ./gwb studio )
}

function deletebranches() {
	echo -e "Deleting branches for $1"
	(cd $1 &&  git fetch -p && for branch in `git branch -vv | grep ': gone]' | awk '{print $1}'`; do git branch -D $branch; done )
}

function runProject() {
	echo -e "Running  $1 on env $run"
	
	if ([ "$1" = "billingcenter" ] && [ -n "$listening" ]) ; then 
		port="$bPortPrefix$listening";
	elif ([ "$1" = "contactmanager" ] && [ -n "$listening" ]) ; then 
		port="$aPortPrefix$listening";
	elif ([ "$1" = "policycenter" ] && [ -n "$listening" ]) ; then 
		port="$pPortPrefix$listening";
	elif ([ "$1" = "claimcenter" ] && [ -n "$listening" ]) ; then 
		port="$cPortPrefix$listening";
	fi
	
	if [ -z ${port} ]; then 
		(cd $1 && ./gwb runserver -Denv="$run")
	else 
		echo -e "On port $port"
		(cd $1 && ./gwb runserver -Denv="$run" -Dgw.port="$port")
	fi
	
}

function stopProject() {
	echo -e "Stopping  $1 on env $stop"
    (cd $1 && ./gwb stopserver -Denv="$stop")
}

if [ "$git" = true ]; then 
  for i in "${arr[@]}"
  do
    if  [ "$all" = true ] || ([ "$i" = "billingcenter" ] && [ "$billing" = true ]) || ([ "$i" = "policycenter" ] && [ "$policy" = true ]) || ([ "$i" = "contactmanager" ] && [ "$contact" = true ]) || ([ "$i" = "claimcenter" ] && [ "$claims" = true ]) ; then
       pullFromGit "$i"
    fi
  done
fi

if [ "$delete" = true ]; then 
  for i in "${arr[@]}"
  do
    if  [ "$all" = true ] || ([ "$i" = "billingcenter" ] && [ "$billing" = true ]) || ([ "$i" = "policycenter" ] && [ "$policy" = true ]) || ([ "$i" = "contactmanager" ] && [ "$contact" = true ]) || ([ "$i" = "claimcenter" ] && [ "$claims" = true ]) ; then
        deletebranches "$i"
    fi
  done
fi

if [ "$ide" = true ]; then 
  for i in "${arr[@]}"
  do
    if  [ "$all" = true ] || ([ "$i" = "billingcenter" ] && [ "$billing" = true ]) || ([ "$i" = "policycenter" ] && [ "$policy" = true ]) || ([ "$i" = "contactmanager" ] && [ "$contact" = true ]) || ([ "$i" = "claimcenter" ] && [ "$claims" = true ]) ; then
      openStudio "$i" &
    fi
  done
  wait
fi

if [ -n "$run" ]; then 
  for i in "${arr[@]}"
  do
    if  [ "$all" = true ] || ([ "$i" = "billingcenter" ] && [ "$billing" = true ]) || ([ "$i" = "policycenter" ] && [ "$policy" = true ]) || ([ "$i" = "contactmanager" ] && [ "$contact" = true ]) || ([ "$i" = "claimcenter" ] && [ "$claims" = true ]) ; then
      runProject "$i" &
    fi
  done
  wait
fi

if [ -n "$stop" ]; then 
  for i in "${arr[@]}"
  do
    if  [ "$all" = true ] || ([ "$i" = "billingcenter" ] && [ "$billing" = true ]) || ([ "$i" = "policycenter" ] && [ "$policy" = true ]) || ([ "$i" = "contactmanager" ] && [ "$contact" = true ]) || ([ "$i" = "claimcenter" ] && [ "$claims" = true ]) ; then
      stopProject "$i" &
    fi
  done
  wait
fi


