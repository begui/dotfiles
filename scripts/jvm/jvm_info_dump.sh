
#!/bin/bash

# ---------------------------------------------------------------------
# Script: jvm_info_dump.sh
# This script collects diagnostic information about a running JVM process.
# It captures the following details:
#  - Thread dump
#  - Heap information
#  - Class histogram
#  - GC finalizer information
#  - VM flags
#  - VM system properties
#  - VM uptime
#
# Usage:
#   ./jvm_info_dump.sh <process_id> [optional_output_directory]
#
# Arguments:
#   process_id                 The process ID (PID) of the target JVM process.
#   optional_output_directory  (Optional) The directory where the output files
#                              will be saved. If not provided, the current
#                              directory will be used.
#
# Example:
#   ./jvm_info_dump.sh 12345 /tmp/jvm_dumps
#
# ---------------------------------------------------------------------

if [ -z "$1" ]; then
  echo "Usage: $0 <process_id> [optional_output_directory]"
  exit 1
fi

PID=$1
OUTPUT_DIR=${2:-"."}  # Default to current directory if not provided

# Check if jcmd is available
if ! command -v jcmd &> /dev/null; then
  echo "Error: jcmd command not found. Ensure it is in your PATH."
  exit 1
fi

TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

mkdir -p "$OUTPUT_DIR"

THREAD_DUMP_FILE="${OUTPUT_DIR}/thread_dump_${PID}_${TIMESTAMP}.txt"
HEAP_INFO_FILE="${OUTPUT_DIR}/heap_info_${PID}_${TIMESTAMP}.txt"
CLASS_HISTOGRAM_FILE="${OUTPUT_DIR}/class_histogram_${PID}_${TIMESTAMP}.txt"
GC_FINALIZER_INFO_FILE="${OUTPUT_DIR}/gc_finalizer_info_${PID}_${TIMESTAMP}.txt"
VM_FLAGS_FILE="${OUTPUT_DIR}/vm_flags_${PID}_${TIMESTAMP}.txt"
VM_PROPERTIES_FILE="${OUTPUT_DIR}/vm_properties_${PID}_${TIMESTAMP}.txt"
UPTIME_FILE="${OUTPUT_DIR}/vm_uptime_${PID}_${TIMESTAMP}.txt"

# Take thread dump
echo "Taking thread dump of process with PID: $PID..."
jcmd $PID Thread.print > $THREAD_DUMP_FILE

# Get heap information
echo "Taking heap information of process with PID: $PID..."
jcmd $PID GC.heap_info > $HEAP_INFO_FILE

# Get class histogram
echo "Taking class histogram of process with PID: $PID..."
jcmd $PID GC.class_histogram > $CLASS_HISTOGRAM_FILE

# Get GC finalizer information
echo "Taking GC finalizer information of process with PID: $PID..."
jcmd $PID GC.finalizer_info > $GC_FINALIZER_INFO_FILE

# Get VM flags
echo "Taking VM flags of process with PID: $PID..."
jcmd $PID VM.flags > $VM_FLAGS_FILE

# Get VM system properties
echo "Taking VM system properties of process with PID: $PID..."
jcmd $PID VM.system_properties > $VM_PROPERTIES_FILE

# Get VM uptime
echo "Taking VM uptime of process with PID: $PID..."
jcmd $PID VM.uptime > $UPTIME_FILE

echo "All information has been saved to the directory: $OUTPUT_DIR"
