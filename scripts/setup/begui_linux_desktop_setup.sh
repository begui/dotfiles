#!/bin/bash

function help() {
    echo "Usage example:";
    echo "begui_linux_setup [(-h|--help)] ";
    echo "Options:";
    echo "-h or --help   : Displays this information.";
    echo "-i or --install: Installs a bunch of stuff.";
    echo "-u or --update : Updates a bunch of stuff.";
}

function begui_install() {
  if command -v dnf > /dev/null; then

    echo "Installing Developer Tools..."
    sudo dnf -y install git cloc valgrind cppcheck cmake vim htop cgdb gcc clang java-latest-openjdk-devel golang

    echo "Installing Developer Libs..."
    sudo dnf -y install nss-tools python3-devel

    echo "Installing gamedev libraries..."
    sudo dnf -y install SDL2{,_image,_mixer,_ttf,_gfx}-devel 
    sudo dnf -y install physfs-devel libpng-devel

    echo "Setting up desktop..."
    gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
    gsettings set org.gnome.desktop.interface enable-hot-corners false
  elif command -v apt > /dev/null; then
    echo "Installing Applications..."
    sudo apt -y install keepassx hexchat filezilla openfortivpn remmina flameshot htop pandoc psensor

    #Game Dev Apps 
    #sudo apt -y install tiled krita gimp

    echo "Installing Developer Tools..."
    sudo apt -y install automake clang cmake clang-format cloc cppcheck vim-gtk3 g++ git gitk cgdb meld exuberant-ctags mesa-utils ninja-build pkg-config valgrind massif-visualizer kcachegrind curl net-tools openjdk-17-jdk maven 

    echo "Installing Developer Libs..."
    sudo apt -y install build-essential gcc-multilib g++-multilib linux-tools-common u-boot-tools {python3,ncurses}-dev libgtk-3-dev liblzma-dev 

    echo "Installing gamedev libraries..."
    sudo apt -y install libsdl2{,-image,-mixer,-ttf,-gfx}-dev lib{assimp,glm,vorbis,theora}-dev 

    #Optional libs
    #sudo apt -y install texlive-full 
  else
    echo "Could not determine the package manager."
    exit 1
  fi

}

function begui_update() {
  if command -v dnf > /dev/null; then
    sudo dnf -y --refresh update && sudo dnf -y upgrade
  elif command -v apt > /dev/null; then
    sudo apt -y update && sudo apt -y upgrade
  else
    echo "Could not determine the package manager."
    exit 1
  fi
}

OPTS=$(getopt -o hui --long help,update,install -n 'test.sh' -- "$@")


# Check for parsing errors
if [ $? != 0 ]; then
  echo "Failed to parse options." >&2
  help
  exit 1
fi

# Set the arguments to the parsed options
eval set -- "$OPTS"

# Set default values for the options
update=0
install=0

# Process the options
while true; do
  case "$1" in
    -u|--update)
      update=1
      shift
      ;;
    -i|--install)
      install=1
      shift
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "Invalid option: $1" >&2
      exit 1
      ;;
  esac
done

if [ $update == 1 ] && [ $install == 1 ]; then
  echo "Updating and installing..."
  begui_update && begui_install
elif [ $update == 1 ]; then
  echo "Updating..."
  begui_update
elif [ $install == 1 ]; then
  echo "Installing..."
  begui_install
else
  echo "No options were specified."
  help
  exit 1
fi


